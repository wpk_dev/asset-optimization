<?php
/**
 * Tools Administration Screen.
 *
 * @package WordPress
 * @subpackage Administration
 */

/** WordPress Administration Bootstrap */
require_once ABSPATH . 'wp-admin/admin.php';

wp_reset_vars( array( 'action' ) );

$tabs = array(
	/* translators: Tab heading for Asset Optimization Status page. */
	''      => sprintf(_x( 'Active Theme (%s)', 'Asset Optimization', 'asset-optimization' ), wp_get_theme()->get('Name')),
	//'settings' => _x( 'Settings', 'Asset Optimization', 'asset-optimization' ),
	/* translators: Tab heading for Asset Optimization Info page. */
	//'debug' => _x( 'Info', 'Asset Optimization', 'asset-optimization' ),
	
);

/**
 * An associated array of extra tabs for the Asset Optimization navigation bar.
 *
 * @param array $tabs An associated array of tab slugs and their label.
 */
$tabs = apply_filters( 'site_health_navigation_tabs', $tabs );

$wrapper_classes = array(
	'asset-optimization-tabs-wrapper',
	'hide-if-no-js',
	'tab-count-' . count( $tabs ),
);

$current_tab = ( isset( $_GET['tab'] ) ? $_GET['tab'] : '' );

$title = sprintf(
	// translators: %s: The currently displayed tab.
	__( 'Asset Optimization - %s' ),
	( isset( $tabs[ $current_tab ] ) ? esc_html( $tabs[ $current_tab ] ) : esc_html( reset( $tabs ) ) )
);

if ( ! current_user_can( 'view_site_health_checks' ) ) {
	wp_die( __( 'Sorry, you are not allowed to access site health information.' ), '', 403 );
}
?>
<div class="asset-optimization-header">
	<div class="asset-optimization-title-section">
		<h1>
			<?php _e( 'Asset Optimization' ); ?>
		</h1>
	</div>

	<?php
	if ( isset( $_GET['https_updated'] ) ) {
		if ( $_GET['https_updated'] ) {
			?>
			<div id="message" class="notice notice-success is-dismissible"><p><?php _e( 'Site URLs switched to HTTPS.' ); ?></p></div>
			<?php
		} else {
			?>
			<div id="message" class="notice notice-error is-dismissible"><p><?php _e( 'Site URLs could not be switched to HTTPS.' ); ?></p></div>
			<?php
		}
	}
	?>

	<div class="asset-optimization-title-section asset-optimization-progress-wrapper loading hide-if-no-js">
		<div class="asset-optimization-progress">
			<svg role="img" aria-hidden="true" focusable="false" width="100%" height="100%" viewBox="0 0 200 200" version="1.1" xmlns="http://www.w3.org/2000/svg">
				<circle r="90" cx="100" cy="100" fill="transparent" stroke-dasharray="565.48" stroke-dashoffset="0"></circle>
				<circle id="bar" r="90" cx="100" cy="100" fill="transparent" stroke-dasharray="565.48" stroke-dashoffset="0"></circle>
			</svg>
		</div>
		<div class="asset-optimization-progress-label">
			<?php _e( 'Results are still loading&hellip;' ); ?>
		</div>
	</div>

	<nav class="<?php echo implode( ' ', $wrapper_classes ); ?>" aria-label="<?php esc_attr_e( 'Secondary menu' ); ?>">
		<?php
		$tabs_slice = $tabs;

		/*
		 * If there are more than 4 tabs, only output the first 3 inline,
		 * the remaining links will be added to a sub-navigation.
		 */
		if ( count( $tabs ) > 4 ) {
			$tabs_slice = array_slice( $tabs, 0, 3 );
		}

		foreach ( $tabs_slice as $slug => $label ) {
			printf(
				'<a href="%s" class="asset-optimization-tab %s">%s</a>',
				esc_url(
					add_query_arg(
						array(
							'tab' => $slug,
						),
						admin_url( 'tools.php?page=asset-optimization' )
					)
				),
				( $current_tab === $slug ? 'active' : '' ),
				esc_html( $label )
			);
		}
		?>

		<?php if ( count( $tabs ) > 4 ) : ?>
			<button type="button" class="asset-optimization-tab asset-optimization-offscreen-nav-wrapper" aria-haspopup="true">
				<span class="dashicons dashicons-ellipsis"></span>
				<span class="screen-reader-text"><?php _e( 'Toggle extra menu items' ); ?></span>

				<div class="asset-optimization-offscreen-nav">
					<?php
					// Remove the first few entries from the array as being already output.
					$tabs_slice = array_slice( $tabs, 3 );
					foreach ( $tabs_slice as $slug => $label ) {
						printf(
							'<a href="%s" class="asset-optimization-tab %s">%s</a>',
							esc_url(
								add_query_arg(
									array(
										'tab' => $slug,
									),
									admin_url( 'tools.php?page=asset-optimization' )
								)
							),
							( isset( $_GET['tab'] ) && $_GET['tab'] === $slug ? 'active' : '' ),
							esc_html( $label )
						);
					}
					?>
				</div>
			</button>
		<?php endif; ?>
	</nav>
</div>

<hr class="wp-header-end">

<?php
if ( isset( $_GET['tab'] ) && ! empty( $_GET['tab'] ) ) {
	/**
	 * Output content of a custom Asset Optimization tab.	
	 *
	 * @param string $tab The slug of the tab that was requested.
	 */
	do_action( 'asset_optimization_tab_content', $_GET['tab'] );

	return;
} else {
	?>

<div class="notice notice-error hide-if-js">
	<p><?php _e( 'The Asset Optimization check requires JavaScript.', 'asset-optimization' ); ?></p>
</div>

<div class="asset-optimization-body asset-optimization-status-tab hide-if-no-js">
	<div class="site-status-all-clear hide">
		<p class="icon">
			<span class="dashicons dashicons-yes"></span>
		</p>

		<p class="encouragement">
			<?php _e( 'Great job!' ); ?>
		</p>

		<p>
			<?php _e( 'Everything is running smoothly here.' ); ?>
		</p>
	</div>

	<div class="site-status-has-issues">
		<h2>
			<?php _e( 'Asset Optimization Status' ); ?>
		</h2>

		<p><?php _e( 'The Asset Optimization Status check shows critical information about your Site and items that require your attention.' ); ?></p>

		<div class="asset-optimization-issues-wrapper" id="asset-optimization-issues-critical">
			<h3 class="asset-optimization-issue-count-title">
				<?php
					/* translators: %s: Number of critical issues found. */
					printf( _n( '%s critical issue', '%s critical issues', 0 ), '<span class="issue-count">0</span>' );
				?>
			</h3>

			<div id="asset-optimization-status-critical" class="asset-optimization-accordion issues"></div>
		</div>

		<div class="asset-optimization-issues-wrapper" id="asset-optimization-issues-recommended">
			<h3 class="asset-optimization-issue-count-title">
				<?php
					/* translators: %s: Number of recommended improvements. */
					printf( _n( '%s recommended improvement', '%s recommended improvements', 0 ), '<span class="issue-count">0</span>' );
				?>
			</h3>

			<div id="asset-optimization-status-recommended" class="asset-optimization-accordion issues"></div>
		</div>
	</div>

	<div class="asset-optimization-view-more">
		<button type="button" class="button asset-optimization-view-passed" aria-expanded="false" aria-controls="asset-optimization-issues-good">
			<?php _e( 'Passed tests' ); ?>
			<span class="icon"></span>
		</button>
	</div>

	<div class="asset-optimization-issues-wrapper hidden" id="asset-optimization-issues-good">
		<h3 class="asset-optimization-issue-count-title">
			<?php
				/* translators: %s: Number of items with no issues. */
				printf( _n( '%s item with no issues detected', '%s items with no issues detected', 0 ), '<span class="issue-count">0</span>' );
			?>
		</h3>

		<div id="asset-optimization-status-good" class="asset-optimization-accordion issues"></div>
	</div>
</div>

<script id="tmpl-asset-optimization-issue" type="text/template">
	<h4 class="asset-optimization-accordion-heading accordian-{{ data.badge.color }}">
		<button aria-expanded="false" class="asset-optimization-accordion-trigger" aria-controls="asset-optimization-accordion-block-{{ data.test }}" type="button">
			<span class="title">{{ data.label }}</span>
			<# if ( data.badge ) { #>
				<span class="badge {{ data.badge.color }}">{{ data.badge.label }}</span>
			<# } #>
			<span class="icon"></span>
		</button>
	</h4>
	<div id="asset-optimization-accordion-block-{{ data.test }}" class="asset-optimization-accordion-panel" hidden="hidden">
		{{{ data.description }}}
		<# if ( data.actions ) { #>
			<div class="actions">
				{{{ data.actions }}}
			</div>
		<# } #>
	</div>
</script>

	<?php
}