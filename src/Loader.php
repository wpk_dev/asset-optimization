<?php
namespace AssetOptimization;

class Loader{


	 /**
     * Autoload method
     * @return void
     */
    public function __construct() {
        add_action( 'init', [ $this, 'init' ] );
        add_action( 'admin_init', [ $this, 'admin_init' ] );    	
        add_action( 'admin_menu', [ &$this, 'register_sub_menu' ] );  
        
           
    }

    public function init(){
        if ( ! defined( 'COMPRESS_SCRIPTS' ) ) define( 'COMPRESS_SCRIPTS'   , true ); 
        if ( ! defined( 'COMPRESS_CSS' ) ) define( 'COMPRESS_CSS'   , true );    
        if ( ! defined( 'AO_PRELOAD_STYLE' ) ) define( 'AO_PRELOAD_STYLE'   , true );    
        if ( ! defined( 'AO_PRELOAD_SCRIPT' ) ) define( 'AO_PRELOAD_SCRIPT'   , true );    

        if( !is_admin() ){
            add_filter( 'style_loader_src', [ $this, 'style_loader_src' ] );  
            add_filter( 'script_loader_src', [ $this, 'style_loader_src' ] );
            add_filter( 'style_loader_tag', [ $this, 'style_loader_tag' ], 10, 4 ); 
            add_filter( 'script_loader_tag', [ $this, 'script_loader_tag' ], 10, 3 ); 
        }
              
    }

    public function style_loader_src($src){        
        if( !COMPRESS_CSS ) return $src;

        $asset_optimization = get_option( 'asset_optimization', array() );
        if(empty($asset_optimization)) return $src;
        $active_theme = Helper::active_theme();
        if(empty($asset_optimization[$active_theme])) return $src;
        $optimized_files = $asset_optimization[$active_theme];

        $optimized_urls = array_combine(
            array_map(function($key){ return get_theme_file_uri($key); }, array_keys($optimized_files)),
            $optimized_files
        );

        $src_url = remove_query_arg('ver', $src);
        if( array_key_exists($src_url, $optimized_urls) ){
            $src = get_theme_file_uri($optimized_urls[$src_url]);
        }
       
        return $src;
    }

    public function style_loader_tag($tag, $handle, $href, $media){ 
        if( !AO_PRELOAD_STYLE ) return $tag;
         $tag = ao_str_get_html($tag);
         $tag->rel = 'preload';
         $tag->as = 'style';


        return $tag;
    }

    public function script_loader_tag($tag, $handle, $src){ 
        if( !AO_PRELOAD_SCRIPT ) return $tag;
         $tag = ao_str_get_html($tag);
         $tag->rel = 'preload';
         $tag->as = 'script';


        return $tag;
    }


    public function admin_init(){
    	new Status;
    	new Optimize;  
             
    }

    /**
     * Register submenu
     * @return void
     */
    public function register_sub_menu() {
        add_submenu_page( 
            'tools.php', 
            esc_attr__('Asset Optimization', 'asset-optimization'), 
            esc_attr__('Asset Optimization', 'asset-optimization'),
            'manage_options', 
            'asset-optimization', 
            array(&$this, 'submenu_page_callback')
        );
    }
 
    /**
     * Render submenu
     * @return void
     */
    public function submenu_page_callback() { 
        include __DIR__ . '/includes/asset-optimization.php';
    }

    

    /**
     * Check if a remote file exists.
     *
     * @param  string $url The url to the remote file.
     * @return bool        Whether the remote file exists.
     */
    private function remote_file_exists( $url ) {
        $response = wp_remote_head( $url );

        return 200 === wp_remote_retrieve_response_code( $response );
    }

    
}