<?php
namespace AssetOptimization;
use AssetOptimization\Helper as Helper;
/**
 * Class for looking up a site's health based on a user's WordPress environment.
 *
 * @package WordPress
 * @subpackage AssetOptimization
 */

class Status {
	private static $instance = null;
	private $type = 'theme';

	private $assets;
	private $files;
	private $folder_name;
	private $folder_path_prefix;

	private $upload_dir;
	
	public $schedules;
	public $crons;
	public $last_missed_cron     = null;
	public $last_late_cron       = null;
	private $timeout_missed_cron = null;
	private $timeout_late_cron   = null;

	/**
	 * Status constructor.
	 */
	public function __construct() {
		

		// Save memory limit before it's affected by wp_raise_memory_limit( 'admin' ).
		$this->php_memory_limit = ini_get( 'memory_limit' );
		$this->config();
		$this->do_ajax();	
			
				

		add_filter( 'admin_body_class', array( $this, 'admin_body_class' ) );
		add_action( 'admin_enqueue_scripts', array( $this, 'enqueue_scripts' ) );
		add_action( 'asset_optimization_tab_content', array( $this, 'show_tab' ) );

	}

	

	/**
	 * Output the content of a tab in the Site Health screen.
	 *
	 * @param string $tab Slug of the current tab being displayed.
	 */
	public function show_tab( $tab ) {
		
		if ( 'plugin' === $tab ) {
			$this->type = 'plugin';
		}
		if ( 'debug' === $tab ) {
			
		}

		if ( 'settings' === $tab ) {
			require_once __DIR__ . '/includes/settings.php';
		}
	}

	/**
	 * Return an instance of the Status class, or create one if none exist yet.
	 *
	 * @return Status|null
	 */
	public static function get_instance() {
		if ( null === self::$instance ) {
			self::$instance = new Status();
		}

		return self::$instance;
	}

	
	/**
	 * Run a Site Health test directly.
	 *
	 * @param callable $callback
	 * @return mixed|void
	 */
	private function perform_test( $callback ) {
		/**
		 * Filters the output of a finished Site Health test.
		 */
		return apply_filters( 'asset_optimization_status_test_result', call_user_func( $callback ) );
	}	

	
	

	/**
	 * Test if CSS Files Size.
	 *
	 * @return array The test results.
	 */
	public function get_test_css_files() {
		$files = $this->assets['css'];
		$debug_data = !empty($files)? $this->debug_log($files, 'css') : '';
		$count = !empty($files)? count($files) : '';

		
		$button = sprintf('<p><a href="#" class="button button-asset-optimize" 
			data-action="%2$s">%1$s</a></p>',
		 	esc_attr__('Optimize CSS Files', 'asset-optimization'),
		 	'css-files'
		);
		$result = array(
			'label'       => sprintf(__( 'CSS Files (%s)', 'asset-optimization' ), $count),
			'status'      => 'critical',
			'badge'       => array(
				'label' => __( 'Performance' ),
				'color' => 'red',
			),
			'description' => sprintf(
				'<p>%1$s</p>%3$s %2$s',				
				__( 'This does all the compression at run-time and is performed by PHP. This essentially means that the web server holds back all the CSS files, then compresses them before delivering them to the browser.' ),
				$debug_data, $button
			),
			'actions'     => '',
			'test'        => 'css_files',
		);	
				

		return $result;
	}

	/**
	 * Test if JavaScripts Files Size.
	 *
	 * @return array The test results.
	 */
	public function get_test_js_files() {
		$files = $this->assets['js'];
		$debug_data = !empty($files)? $this->debug_log($files, 'js') : '';
		$count = !empty($files)? count($files) : '';
		
		$button = sprintf('<p><a href="#" class="button button-asset-optimize" 
			data-action="%2$s">%1$s</a></p>',
		 	esc_attr__('Optimize Javascripts Files', 'asset-optimization'),
		 	'js-files'
		);

		
		$result = array(
			'label'       => sprintf(__( 'JavaScripts Files (%s)', 'asset-optimization' ), $count),
			'status'      => 'recommended',
			'badge'       => array(
				'label' => __( 'Performance' ),
				'color' => 'orange',
			),
			'description' => sprintf(
				'<p>%1$s</p>%3$s %2$s',	
				__( 'This does all the compression at run-time and is performed by PHP. This essentially means that the web server holds back all the JavaScript files, then compresses them before delivering them to the browser.' ),
				$debug_data, $button
			),
			'actions'     => '',
			'test'        => 'js_files',
		);		

		return $result;
	}

	/**
	 * Test if JS Files Size.
	 *
	 * @return array The test results.
	 */
	public function get_test_image_files() {
		$files = self::get_theme_files('png');
		$files = array_merge($files, self::get_theme_files('jpg'));
		$files = array_merge($files, self::get_theme_files('gif'));
		$files = array_merge($files, self::get_theme_files('svg'));

		$result = array(
			'label'       => sprintf(__( 'Images Files (%s)', 'asset-optimization' ), count($files)),
			'status'      => 'good',
			'badge'       => array(
				'label' => __( 'Performance' ),
				'color' => 'green',
			),
			'description' => sprintf(
				'<p>%s</p>',
				__( 'It is possible for site maintainers to block all, or some, communication to other sites and services. If set up incorrectly, this may prevent plugins and themes from working as intended.' )
			),
			'actions'     => '',
			'test'        => 'image_files'
		);		

		return $result;
	}	
	

	/**
	 * Return a set of tests that belong to the site status page.
	 *
	 * Each site status test is defined here, they may be `direct` tests, that run on page load, or `async` tests
	 * which will run later down the line via JavaScript calls to improve page performance and hopefully also user
	 * experiences.
	 *
	 * Added support for `has_rest` and `permissions`.
	 *
	 * @return array The list of tests to run.
	 */
	public static function get_tests() {
		$tests = array(
			'direct' => array(
				'css_files'             => array(
					'label' => __( 'CSS Files' ),
					'test'  => 'css_files',
				),
				'js_files'             => array(
					'label' => __( 'JavaScript Files' ),
					'test'  => 'js_files',
				),
				'image_files'             => array(
					'label' => __( 'Images' ),
					'test'  => 'image_files',
				),
			),
			'async'  => array(
				// TODO				
			),
		);
		
		$tests = apply_filters( 'asset_optimization_status_tests', $tests );

		// Ensure that the filtered tests contain the required array keys.
		$tests = array_merge(
			array(
				'direct' => array(),
				'async'  => array(),
			),
			$tests
		);

		return $tests;
	}

	/**
	 * Enqueues the site health scripts.
	 */
	public function enqueue_scripts() {
		if ( ! $this->is_screen() ) {
			return;
		}

		wp_enqueue_style( 'asset-optimization', ASSET_OPTIMIZATION_URL . 'assets/css/admin.css', ASSET_OPTIMIZATION_VER );
		wp_enqueue_script( 'asset-optimization', ASSET_OPTIMIZATION_URL . 'assets/js/admin.js', ['wp-url', 'wp-i18n', 'common', 'wp-backbone', 'wp-a11y', 'wp-api-request', 'wp-backbone', 'clipboard'], ASSET_OPTIMIZATION_VER, true );

		$js_variables = array(
			'screen'      => get_current_screen()->id,
			'ajaxurl'      => admin_url( 'admin-ajax.php' ),
			'nonce'       => array(
				'asset_optimization'        => wp_create_nonce( 'asset-optimization' ),
				'asset_optimization_status'        => wp_create_nonce( 'asset-optimization-status' ),
				'asset_optimization_status_result' => wp_create_nonce( 'asset-optimization-status-result' ),
			),
			'asset_optimization_status' => array(
				'direct' => array(),
				'async'  => array(),
				'issues' => array(
					'good'        => 0,
					'recommended' => 0,
					'critical'    => 0,
				),
			),
			'loading' => esc_attr__( 'Working..', 'asset-optimization' ),
			'copyText' => esc_attr__( 'Save Files in the Active Theme', 'asset-optimization' ),
		);

		$issue_counts = get_transient( 'asset-optimization-status-result' );

		if ( false !== $issue_counts ) {
			$issue_counts = json_decode( $issue_counts );

			$js_variables['asset_optimization_status']['issues'] = $issue_counts;
		}

		if( ! isset( $_GET['tab'] ) || empty( $_GET['tab'] ) )  {
			$tests = Status::get_tests();


			foreach ( $tests['direct'] as $test ) {
				if ( is_string( $test['test'] ) ) {
					$test_function = sprintf(
						'get_test_%s',
						$test['test']
					);

					if ( method_exists( $this, $test_function ) && is_callable( array( $this, $test_function ) ) ) {
						$js_variables['asset_optimization_status']['direct'][] = $this->perform_test( array( $this, $test_function ) );
						continue;
					}
				}

				if ( is_callable( $test['test'] ) ) {
					$js_variables['asset_optimization_status']['direct'][] = $this->perform_test( $test['test'] );
				}
			}

			foreach ( $tests['async'] as $test ) {
				if ( is_string( $test['test'] ) ) {
					$js_variables['asset_optimization_status']['async'][] = array(
						'test'      => $test['test'],
						'has_rest'  => ( isset( $test['has_rest'] ) ? $test['has_rest'] : false ),
						'completed' => false,
						'headers'   => isset( $test['headers'] ) ? $test['headers'] : array(),
					);
				}
			}
		}

		wp_localize_script( 'asset-optimization', 'AssetOptimization', $js_variables );
	}

	public function do_ajax(){
       
        $core_actions = array(
            'asset-optimization-status-result'
        );

        foreach ($core_actions as $key => $action) {
            add_action( 'wp_ajax_' . $action, [$this, str_replace( '-', '_', $action )], 1 );
        }

    }
    /**
     * Ajax handler for site health check to update the result status.
     *
    */
    public function asset_optimization_status_result() {
    

        if ( ! current_user_can( 'view_site_health_checks' ) ) {
            wp_send_json_error();
        }

        set_transient( 'asset-optimization-status-result', wp_json_encode( $_POST['counts'] ) );

        wp_send_json_success();
    }


	/**
	 * Add a class to the body HTML tag.
	 *
	 * Filters the body class string for admin pages and adds our own class for easier styling.
	 *
	 * @param string $body_class The body class string.
	 * @return string The modified body class string.
	 */
	public function admin_body_class( $body_class ) {
		if ( ! $this->is_screen() ) {
			return;
		}

		$body_class .= ' asset-optimization';

		return $body_class;
	}

	/**
	 * Return files in the theme's directory.
	 *
	 * @param string[]|string $type          Optional. Array of extensions to find, string of a single extension,
	 *                                       or null for all extensions. Default null.
	 * @param int             $depth         Optional. How deep to search for files. Defaults to a flat scan (0 depth).
	 *                                       -1 depth is infinite.
	 * @param bool            $search_parent Optional. Whether to return parent files. Default false.
	 * @return string[] Array of files, keyed by the path to the file relative to the theme's directory, with the values
	 *                  being absolute paths.
	 */
	public static function get_theme_files( $type = null, $depth = 3, $search_parent = true ) {
		$files = (array) self::scandir( get_stylesheet_directory(), $type, $depth );

		if ( $search_parent ) {
			$files += (array) self::scandir( get_template_directory(), $type, $depth );
		}

		return $files;
	}

	/**
	 * Return files in the plugin's directory.
	 * 
	 * @param string            $plugin required. Plugin directory/folder name
	 *
	 * @param string[]|string $type          Optional. Array of extensions to find, string of a single extension,
	 *                                       or null for all extensions. Default null.
	 * @param int             $depth         Optional. How deep to search for files. Defaults to a flat scan (0 depth).
	 *                                       -1 depth is infinite.
	 * @return string[] Array of files, keyed by the path to the file relative to the theme's directory, with the values
	 *                  being absolute paths.
	 */
	public static function get_plugin_files( $plugin, $type = null, $depth = 3 ) {
		$files = (array) self::scandir( WP_PLUGIN_DIR . '/'.$plugin, $type, $depth );

		

		return $files;
	}

	/**
	 * Return files in the asset-optimization directory.
	 * 
	 * @param string            $directory required. Plugin folder name
	 *
	 * @param string[]|string $type          Optional. Array of extensions to find, string of a single extension,
	 *                                       or null for all extensions. Default null.
	 * @param int             $depth         Optional. How deep to search for files. Defaults to a flat scan (0 depth).
	 *                                       -1 depth is infinite.
	 * @return string[] Array of files, keyed by the path to the file relative to the theme's directory, with the values
	 *                  being absolute paths.
	 */
	public static function get_files( $directory, $type = null, $depth = 3 ) {
		$files = (array) self::scandir(WP_CONTENT_DIR.'/uploads/asset-optimization' . '/'.$directory, $type, $depth );

		return $files;
	}

	/**
	* Prepare assets files by extensions
	*
	* @return [] Array of files
	*/
	private function assets( $type = null ){
		$_assets = get_option( 'asset_optimization', array() );
		if( empty($_assets) ) return [];
		$assets = [];
		foreach ($_assets as $folder => $files) {
			if( empty($files) ) continue;

			foreach ($files as $relative_path => $relative_path_min) {
				if( empty($relative_path ) || empty($relative_path_min) ) continue;

				$pathinfo = pathinfo($relative_path_min);
				if( empty($pathinfo['extension']) ) continue;

				$extension = $pathinfo['extension'];

				if( in_array($extension, ['css', 'js']) ){
					$assets[$folder][$extension][$relative_path] = $relative_path_min;
				}else{
					$assets[$folder]['image'][$relative_path] = $relative_path_min;
				}				
			}
		}

		

		return $assets;
	}

	public function config(){

		$assets = $this->assets();

		
		$tab = !empty($_GET['tab'])? $_GET['tab'] : 'default';

		
		switch ($tab) {
			case 'plugins':
				$this->type = 'plugin';
				$this->type = !empty($_GET['plugin'])? $_GET['plugin'] : '';
				$this->folder_path_prefix = trailingslashit(WP_PLUGIN_DIR.'plugins/'.$this->folder_name);
				break;
			
			default:
				$this->type = 'theme';
				$this->folder_name = Helper::active_theme();
				$this->folder_path_prefix = trailingslashit(WP_CONTENT_DIR.'themes/'.$this->folder_name);
				$this->upload_dir = trailingslashit(WP_CONTENT_DIR.'/uploads/asset-optimization/'.$this->folder_name);
				break;
		}

		$default = array(
			'css' => [],
			'js' => [],
			'image' => []
		);

		
		$assets = !empty($assets[$this->folder_name])? $assets[$this->folder_name] : $default;

		
		$this->assets = wp_parse_args($assets, $default);
		
	}

	private function debug_log($files = [], $extension = 'css'){
		
		if( empty($files) ) return;

		$total = $total_min = $size_min = 0;
		$active_theme = $this->folder_name;
		$upload_dir = $this->upload_dir;
		if( !is_dir($upload_dir) ) return;
		ob_start();
		?>
			<table class="widefat striped" role="presentation">
				<?php 
				printf( '<thead><tr><td>%1$s</td><td>%2$s</td><td>%3$s</td></tr></thead>', 
					esc_attr__('Relateive Path', 'asset-optimization'), 
					esc_attr__('Size', 'asset-optimization'), 
					esc_attr__('Size (.min)', 'asset-optimization'), 
				); 
				?>
				<tbody>
				<?php				
				foreach ( $files as $relative_path => $relative_path_min ) {
					$absolute_path = get_template_directory().'/'.$relative_path;
					$absolute_path_min = $upload_dir.$relative_path_min;
					$path_parts = pathinfo($absolute_path);

					if( $path_parts['extension'] != $extension) continue;

					
					if( file_exists($absolute_path_min) ){
						$size_min = filesize($absolute_path_min);						
					}					
					$size = filesize($absolute_path);	
						

					if( $size_min < $size ){
						$total_min += $size_min;
						$total += $size;
						printf( 
							'<tr><td>%1$s</td><td>%2$s</td><td>%3$s</td></tr>', 
							$relative_path_min, 
							size_format($size),
							size_format($size_min),

						);
					}		
					
				}
				?>
				</tbody>
				<?php 
				$save = $total - $total_min;
				printf( '<tfoot><tr><td>%3$s</td><td>%1$s</td><td>%2$s</td></tr></tfoot>', 
					size_format($total, 2), 
					size_format($total_min, 2), 
					sprintf( esc_attr__('Optimized: %s', 'asset-optimization'), size_format($save)), 
					sprintf( esc_attr__('Copy to %s', 'asset-optimization'), wp_get_theme()->get('Name')),				
					'copy-files'	
				); 
				?>
			</table>
		<?php
		return ob_get_clean();

	}


	/**
	 * Scans a directory for files of a certain extension.
	 *
	 * @param string            $path          Absolute path to search.
	 * @param array|string|null $extensions    Optional. Array of extensions to find, string of a single extension,
	 *                                         or null for all extensions. Default null.
	 * @param int               $depth         Optional. How many levels deep to search for files. Accepts 0, 1+, or
	 *                                         -1 (infinite depth). Default 0.
	 * @param string            $relative_path Optional. The basename of the absolute path. Used to control the
	 *                                         returned path for the found files, particularly when this function
	 *                                         recurses to lower depths. Default empty.
	 * @return string[]|false Array of files, keyed by the path to the file relative to the `$path` directory prepended
	 *                        with `$relative_path`, with the values being absolute paths. False otherwise.
	 */
	private static function scandir( $path, $extensions = null, $depth = 0, $relative_path = '' ) {
		if ( ! is_dir( $path ) ) {
			return false;
		}

		if ( $extensions ) {
			$extensions  = (array) $extensions;
			$_extensions = implode( '|', $extensions );
		}

		$relative_path = trailingslashit( $relative_path );
		if ( '/' === $relative_path ) {
			$relative_path = '';
		}

		$results = scandir( $path );
		$files   = array();

		/**
		 * Filters the array of excluded directories and files while scanning theme folder.
		 *
		 * @param string[] $exclusions Array of excluded directories and files.
		 */
		$exclusions = (array) apply_filters( 'asset_optimization_scandir_exclusions', array( 'CVS', 'node_modules', 'vendor', 'bower_components' ) );

		foreach ( $results as $result ) {
			if ( '.' === $result[0] || in_array( $result, $exclusions, true ) ) {
				continue;
			}
			if ( is_dir( $path . '/' . $result ) ) {
				if ( ! $depth ) {
					continue;
				}
				$found = self::scandir( $path . '/' . $result, $extensions, $depth - 1, $relative_path . $result );
				$files = array_merge_recursive( $files, $found );
			} elseif ( ! $extensions || preg_match( '~\.(' . $_extensions . ')$~', $result ) ) {
				$files[ $relative_path . $result ] = $path . '/' . $result;
			}
		}

		return $files;
	}


	private function is_screen() {
		return 'tools_page_asset-optimization' === get_current_screen()->id;
	}

}
