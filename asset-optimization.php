<?php
/**
 * Plugin Name: Asset Optimization
 * Plugin URI: https://senseflame.com/plugins/asset-optimization
 * Description: Optimized your theme assets - images, css & javascript files
 * Author: SenseFlame
 * Author URI: https://senseflame.com
 * Version: 1.0.0
 * Text Domain: asset-optimization
 * Domain Path: /languages
 * Package: AssetOptimization
 * License: GPL v2 - http://www.gnu.org/licenses/old-licenses/gpl-2.0.html
 */

// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) exit;

/**
 * Define constants
 *
 * @since 1.0.0
 */
if ( ! defined( 'ASSET_OPTIMIZATION_VER' ) ) define( 'ASSET_OPTIMIZATION_VER', '1.0' ); 
if ( ! defined( 'ASSET_OPTIMIZATION' ) ) define( 'ASSET_OPTIMIZATION', trim( dirname( plugin_basename( __FILE__ ) ), '/' ) ); 
if ( ! defined( 'ASSET_OPTIMIZATION_DIR' ) ) define( 'ASSET_OPTIMIZATION_DIR'	, plugin_dir_path( __FILE__ ) );
if ( ! defined( 'ASSET_OPTIMIZATION_URL' ) ) define( 'ASSET_OPTIMIZATION_URL'   , plugin_dir_url( __FILE__ ) ); 





if ( ! function_exists( 'asset_optimization_load' ) ) {
    // Load everything
    require_once( ASSET_OPTIMIZATION_DIR . 'vendor/autoload.php' );

	add_action( 'init', 'asset_optimization_load', 1 );

	function asset_optimization_load() {     

		new AssetOptimization\Loader;		
	}   
    
}
