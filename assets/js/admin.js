/**
 * Interactions used by the Site Health modules in WordPress.
 *
 * @output wp-admin/js/asset-optimization.js
 */

/* global ajaxurl, ClipboardJS, AssetOptimization, wp */

jQuery(function($) {

    var __ = wp.i18n.__,
        _n = wp.i18n._n,
        sprintf = wp.i18n.sprintf,
        clipboard = new ClipboardJS('.asset-optimization-copy-buttons .copy-button'),
        isStatusTab = $('.asset-optimization-body.asset-optimization-status-tab').length,
        isDebugTab = $('.asset-optimization-body.asset-optimization-debug-tab').length,
        pathsSizesSection = $('#asset-optimization-accordion-block-wp-paths-sizes'),
        successTimeout;

    // Debug information copy section.
    clipboard.on('success', function(e) {
        var triggerElement = $(e.trigger),
            successElement = $('.success', triggerElement.closest('div'));

        // Clear the selection and move focus back to the trigger.
        e.clearSelection();
        // Handle ClipboardJS focus bug, see https://github.com/zenorocha/clipboard.js/issues/680
        triggerElement.trigger('focus');

        // Show success visual feedback.
        clearTimeout(successTimeout);
        successElement.removeClass('hidden');

        // Hide success visual feedback after 3 seconds since last success.
        successTimeout = setTimeout(function() {
            successElement.addClass('hidden');
            // Remove the visually hidden textarea so that it isn't perceived by assistive technologies.
            if (clipboard.clipboardAction.fakeElem && clipboard.clipboardAction.removeFake) {
                clipboard.clipboardAction.removeFake();
            }
        }, 3000);

        // Handle success audible feedback.
        wp.a11y.speak(__('Site information has been copied to your clipboard.'));
    });

    // Accordion handling in various areas.
    $('.asset-optimization-accordion').on('click', '.asset-optimization-accordion-trigger', function() {
        var isExpanded = ('true' === $(this).attr('aria-expanded'));

        if (isExpanded) {
            $(this).attr('aria-expanded', 'false');
            $('#' + $(this).attr('aria-controls')).attr('hidden', true);
        } else {
            $(this).attr('aria-expanded', 'true');
            $('#' + $(this).attr('aria-controls')).attr('hidden', false);
        }
    });

    // Site Health test handling.

    $('.asset-optimization-view-passed').on('click', function() {
        var goodIssuesWrapper = $('#asset-optimization-issues-good');

        goodIssuesWrapper.toggleClass('hidden');
        $(this).attr('aria-expanded', !goodIssuesWrapper.hasClass('hidden'));
    });

    /**
     * Validates the Site Health test result format.
     *
     * @since 5.6.0
     *
     * @param {Object} issue
     *
     * @return {boolean}
     */
    function validateIssueData(issue) {
        // Expected minimum format of a valid AssetOptimization test response.
        var minimumExpected = {
                test: 'string',
                label: 'string',
                description: 'string'
            },
            passed = true,
            key, value, subKey, subValue;

        // If the issue passed is not an object, return a `false` state early.
        if ('object' !== typeof(issue)) {
            return false;
        }

        // Loop over expected data and match the data types.
        for (key in minimumExpected) {
            value = minimumExpected[key];

            if ('object' === typeof(value)) {
                for (subKey in value) {
                    subValue = value[subKey];

                    if ('undefined' === typeof(issue[key]) ||
                        'undefined' === typeof(issue[key][subKey]) ||
                        subValue !== typeof(issue[key][subKey])
                    ) {
                        passed = false;
                    }
                }
            } else {
                if ('undefined' === typeof(issue[key]) ||
                    value !== typeof(issue[key])
                ) {
                    passed = false;
                }
            }
        }

        return passed;
    }

    /**
     * Appends a new issue to the issue list.
     *
     * @param {Object} issue The issue data.
     */
    function appendIssue(issue) {
        var template = wp.template('asset-optimization-issue'),
            issueWrapper = $('#asset-optimization-issues-' + issue.status),
            heading,
            count;

        /*
         * Validate the issue data format before using it.
         * If the output is invalid, discard it.
         */
        if (!validateIssueData(issue)) {
            return false;
        }

        AssetOptimization.asset_optimization_status.issues[issue.status]++;

        count = AssetOptimization.asset_optimization_status.issues[issue.status];

        // If no test name is supplied, append a placeholder for markup references.
        if (typeof issue.test === 'undefined') {
            issue.test = issue.status + count;
        }

        if ('critical' === issue.status) {
            heading = sprintf(
                _n('%s critical issue', '%s critical issues', count),
                '<span class="issue-count">' + count + '</span>'
            );
        } else if ('recommended' === issue.status) {
            heading = sprintf(
                _n('%s recommended improvement', '%s recommended improvements', count),
                '<span class="issue-count">' + count + '</span>'
            );
        } else if ('good' === issue.status) {
            heading = sprintf(
                _n('%s item with no issues detected', '%s items with no issues detected', count),
                '<span class="issue-count">' + count + '</span>'
            );
        }

        if (heading) {
            $('.asset-optimization-issue-count-title', issueWrapper).html(heading);
        }

        $('.issues', '#asset-optimization-issues-' + issue.status).append(template(issue));
    }

    /**
     * Updates site health status indicator as asynchronous tests are run and returned.
     *
     */
    function recalculateProgression() {
        var r, c, pct;
        var $progress = $('.asset-optimization-progress');
        var $wrapper = $progress.closest('.asset-optimization-progress-wrapper');
        var $progressLabel = $('.asset-optimization-progress-label', $wrapper);
        var $circle = $('.asset-optimization-progress svg #bar');
        var totalTests = parseInt(AssetOptimization.asset_optimization_status.issues.good, 0) +
            parseInt(AssetOptimization.asset_optimization_status.issues.recommended, 0) +
            (parseInt(AssetOptimization.asset_optimization_status.issues.critical, 0) * 1.5);
        var failedTests = (parseInt(AssetOptimization.asset_optimization_status.issues.recommended, 0) * 0.5) +
            (parseInt(AssetOptimization.asset_optimization_status.issues.critical, 0) * 1.5);
        var val = 100 - Math.ceil((failedTests / totalTests) * 100);

        if (0 === totalTests) {
            $progress.addClass('hidden');
            return;
        }

        $wrapper.removeClass('loading');

        r = $circle.attr('r');
        c = Math.PI * (r * 2);

        if (0 > val) {
            val = 0;
        }
        if (100 < val) {
            val = 100;
        }

        pct = ((100 - val) / 100) * c + 'px';

        $circle.css({ strokeDashoffset: pct });

        if (1 > parseInt(AssetOptimization.asset_optimization_status.issues.critical, 0)) {
            $('#asset-optimization-issues-critical').addClass('hidden');
        }

        if (1 > parseInt(AssetOptimization.asset_optimization_status.issues.recommended, 0)) {
            $('#asset-optimization-issues-recommended').addClass('hidden');
        }

        if (80 <= val && 0 === parseInt(AssetOptimization.asset_optimization_status.issues.critical, 0)) {
            $wrapper.addClass('green').removeClass('orange');

            $progressLabel.text(__('Good'));
            wp.a11y.speak(__('All Assets Optimization tests have finished running. Your site is looking good, and the results are now available on the page.'));
        } else {
            $wrapper.addClass('orange').removeClass('green');

            $progressLabel.text(__('Should be improved'));
            wp.a11y.speak(__('All Assets Optimization tests have finished running. There are items that should be addressed, and the results are now available on the page.'));
        }

        if (isStatusTab) {
            $.post(
                ajaxurl, {
                    'action': 'asset-optimization-status-result',
                    '_wpnonce': AssetOptimization.nonce.asset_optimization_status_result,
                    'counts': AssetOptimization.asset_optimization_status.issues
                }
            );

            if (100 === val) {
                $('.site-status-all-clear').removeClass('hide');
                $('.site-status-has-issues').addClass('hide');
            }
        }
    }

    /**
     * Queues the next asynchronous test when we're ready to run it.
     */
    function maybeRunNextAsyncTest() {
        var doCalculation = true;

        if (1 <= AssetOptimization.asset_optimization_status.async.length) {
            $.each(AssetOptimization.asset_optimization_status.async, function() {
                var data = {
                    'action': 'asset-optimization-' + this.test.replace('_', '-'),
                    '_wpnonce': AssetOptimization.nonce.asset_optimization_status
                };

                if (this.completed) {
                    return true;
                }

                doCalculation = false;

                this.completed = true;

                if ('undefined' !== typeof(this.has_rest) && this.has_rest) {
                    wp.apiRequest({
                            url: wp.url.addQueryArgs(this.test, { _locale: 'user' }),
                            headers: this.headers
                        })
                        .done(function(response) {
                            appendIssue(wp.hooks.applyFilters('asset_optimization_status_test_result', response));
                        })
                        .fail(function(response) {
                            var description;

                            if ('undefined' !== typeof(response.responseJSON) && 'undefined' !== typeof(response.responseJSON.message)) {
                                description = response.responseJSON.message;
                            } else {
                                description = __('No details available');
                            }

                            addFailedAssetOptimizationCheckNotice(this.url, description);
                        })
                        .always(function() {
                            maybeRunNextAsyncTest();
                        });
                } else {
                    $.post(
                        ajaxurl,
                        data
                    ).done(function(response) {
                        appendIssue(wp.hooks.applyFilters('asset_optimization_status_test_result', response.data));
                    }).fail(function(response) {
                        var description;

                        if ('undefined' !== typeof(response.responseJSON) && 'undefined' !== typeof(response.responseJSON.message)) {
                            description = response.responseJSON.message;
                        } else {
                            description = __('No details available');
                        }

                        addFailedAssetOptimizationCheckNotice(this.url, description);
                    }).always(function() {
                        maybeRunNextAsyncTest();
                    });
                }

                return false;
            });
        }

        if (doCalculation) {
            recalculateProgression();
        }
    }

    /**
     * Add the details of a failed asynchronous test to the list of test results.
     */
    function addFailedAssetOptimizationCheckNotice(url, description) {
        var issue;

        issue = {
            'status': 'recommended',
            'label': __('A test is unavailable'),
            'badge': {
                'color': 'red',
                'label': __('Unavailable')
            },
            'description': '<p>' + url + '</p><p>' + description + '</p>',
            'actions': ''
        };

        /** This filter is documented in wp-admin/includes/class-wp-asset-optimization.php */
        appendIssue(wp.hooks.applyFilters('asset_optimization_status_test_result', issue));
    }

    if ('undefined' !== typeof AssetOptimization) {
        if (0 === AssetOptimization.asset_optimization_status.direct.length && 0 === AssetOptimization.asset_optimization_status.async.length) {
            recalculateProgression();
        } else {
            AssetOptimization.asset_optimization_status.issues = {
                'good': 0,
                'recommended': 0,
                'critical': 0
            };
        }

        if (0 < AssetOptimization.asset_optimization_status.direct.length) {
            $.each(AssetOptimization.asset_optimization_status.direct, function() {
                appendIssue(this);
            });
        }

        if (0 < AssetOptimization.asset_optimization_status.async.length) {
            maybeRunNextAsyncTest();
        } else {
            recalculateProgression();
        }
    }

    function getDirectorySizes() {
        var timestamp = (new Date().getTime());

        // After 3 seconds announce that we're still waiting for directory sizes.
        var timeout = window.setTimeout(function() {
            wp.a11y.speak(__('Please wait...'));
        }, 3000);

        wp.apiRequest({
            path: '/wp-asset-optimization/v1/directory-sizes'
        }).done(function(response) {
            updateDirSizes(response || {});
        }).always(function() {
            var delay = (new Date().getTime()) - timestamp;

            $('.asset-optimization-wp-paths-sizes.spinner').css('visibility', 'hidden');
            recalculateProgression();

            if (delay > 3000) {
                /*
                 * We have announced that we're waiting.
                 * Announce that we're ready after giving at least 3 seconds
                 * for the first announcement to be read out, or the two may collide.
                 */
                if (delay > 6000) {
                    delay = 0;
                } else {
                    delay = 6500 - delay;
                }

                window.setTimeout(function() {
                    wp.a11y.speak(__('All Assets Optimization tests have finished running.'));
                }, delay);
            } else {
                // Cancel the announcement.
                window.clearTimeout(timeout);
            }

            $(document).trigger('asset-optimization-info-dirsizes-done');
        });
    }

    function updateDirSizes(data) {
        var copyButton = $('button.button.copy-button');
        var clipboardText = copyButton.attr('data-clipboard-text');

        $.each(data, function(name, value) {
            var text = value.debug || value.size;

            if (typeof text !== 'undefined') {
                clipboardText = clipboardText.replace(name + ': loading...', name + ': ' + text);
            }
        });

        copyButton.attr('data-clipboard-text', clipboardText);

        pathsSizesSection.find('td[class]').each(function(i, element) {
            var td = $(element);
            var name = td.attr('class');

            if (data.hasOwnProperty(name) && data[name].size) {
                td.text(data[name].size);
            }
        });
    }

    if (isDebugTab) {
        if (pathsSizesSection.length) {
            getDirectorySizes();
        } else {
            recalculateProgression();
        }
    }

    // Trigger a class toggle when the extended menu button is clicked.
    $('.asset-optimization-offscreen-nav-wrapper').on('click', function() {
        $(this).toggleClass('visible');
    });

    // Trigger a class toggle when the extended menu button is clicked.
    $('.button-asset-optimize').on('click', function() {
        var action = $(this).data('action');
        if( action == '' ){
            return false;
        }
        var button = $(this);
        var buttonText = $(this).text();
        button.text(AssetOptimization.loading);
        var data = {
            'action': 'asset_optimization',
            'class': $(this).data('action'),
            'nonce': AssetOptimization.nonce.asset_optimization
        };


        $.post(AssetOptimization.ajaxurl, data, function(response) { 
                     
            if( typeof response === 'object' &&  response.success !== null ){
                var results = response.data;
                button.text(results.btntext);
                if( results.action !== null ){
                    button.data('action', results.action)
                }else{
                    button.off('click');
                    button.removeAttr('data-action');
                }
            }
            
        });
        return false;
    });
});